var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var haml = require('gulp-haml');
var cache = require('gulp-cache');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');

/* Development and production directories*/
var appDir = 'static_templates/app';
var devDir = 'static_templates/dev';

gulp.task('browser-sync', function() {
	  browserSync({
	    server: {
	      baseDir: appDir
	    },
	  })
	})
	
/*sass task: compile Sass into CSS and reload browser*/
gulp.task('sass', function() {
	  return gulp.src(devDir+'/sass/**/*.scss') // Gets all files ending with .scss in app/scss
	    .pipe(sass())
        .pipe(cssnano())
	    .pipe(gulp.dest(appDir+'/css'))
	    .pipe(browserSync.reload({stream: true}))
});

gulp.task('html', function() {
	  return gulp.src(devDir+'/*.html')
	  .pipe(gulp.dest(appDir+'/'))
	  .pipe(browserSync.reload({stream: true}))
})

/*haml task: compile*/
gulp.task('haml', function() {
	  return gulp.src(devDir+'/haml/**/*.haml') 
	    .pipe(haml())
    	.pipe(gulp.dest(appDir+'/'))
	    .pipe(browserSync.reload({stream: true}))
});

/*haml task: compile*/
gulp.task('js', function() {
	  return gulp.src(devDir+'/js/**/*.js') 
        .pipe(uglify())
	    .pipe(gulp.dest(appDir+'/js'))
	    .pipe(browserSync.reload({stream: true}))
});

/*img task: Optimizing Images*/

gulp.task('img', function(){
	  return gulp.src(devDir+'/img/**/*.+(png|jpg|jpeg|gif|svg)')
	  .pipe(cache(imagemin({
	      interlaced: true
	    })))
	  .pipe(gulp.dest(appDir+'/img'))
});

/*Migrations*/
gulp.task('migrate-html', function() {
	  return gulp.src(devDir+'/*.html')
	  .pipe(gulp.dest(appDir+'/'))
})
gulp.task('migrate-css', function() {
	  return gulp.src(devDir+'/css/**/*')
	  .pipe(gulp.dest(appDir+'/css'))
})
gulp.task('migrate-js', function() {
	  return gulp.src(devDir+'/fonts/**/*')
	  .pipe(gulp.dest(appDir+'/fonts'))
})
gulp.task('migrate-fonts', function() {
	  return gulp.src(devDir+'/fonts/**/*')
	  .pipe(gulp.dest(appDir+'/css/fonts'))
})

/*watch task: run tasks when html,js css changes occurs*/
gulp.task('migrate-all', ['migrate-html','migrate-css','migrate-js','migrate-fonts']);

/*watch task: run tasks when html,js css changes occurs*/
gulp.task('watch', ['html','sass','js','img','browser-sync'], function (){
	  gulp.watch(devDir+'/sass/**/*.scss', ['sass']); 
	  gulp.watch(devDir+'/*.html', ['html']); 
	  gulp.watch(devDir+'/js/**/*.js', ['js']); 
});


