# README #

adonisdimas website project. The source files of adonisdimas.com website project.
A personal website that highlights my work.

*Static templates*

To build the static templates of the project you need nodejs and npm installed on your computer.
Open the command line and run the following in the root of the folder.

* `npm install`
* `npm init`
* `npm install -g bower`
* `npm install --global gulp`

Development workflow:

* Run the gulp development server: `gulp watch`

Frequest tasks

* Add bower dependency: run `bower install [depedency name] --save`, then run `bower-installer`. You can load your depedency in html with the path `"vendor/[depedency name]/somefile.js"`
* Remove bower dependency: delete the depedency from `bower.json`, run `gulp clean:bower`, the run the above commands.

References:

* [npm](https://www.npmjs.com/)
* [gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* [bower](http://bower.io/#getting-started) 
* [bower-installer](https://github.com/blittle/bower-installer) 
* [sass](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) 
* [haml](http://haml.info/docs/yardoc/file.REFERENCE.html) 

